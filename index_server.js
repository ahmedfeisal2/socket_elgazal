var server = require('http').Server()
var io = require('socket.io')(server)
var request = require('ajax-request')


// var Redis = require('ioredis')
// var redis = new Redis()

io.on('connection', function (socket) {
    console.log('new user connected')
    socket.on('emit_location', function (msg) {
        console.log(msg)

        response_type = typeof msg
        if (response_type == 'string') {
            var myobj = JSON.parse(msg)
            var output = {}
            output['event_name'] = myobj['event_name']
            output['order_id'] = myobj['order_id']
            output['lat'] = myobj['lat']
            output['lng'] = myobj['lng']
        }
        io.emit(myobj['event_name'], output)
    })
    socket.on('update_location', function (msg) {
        // console.log(msg)

        var re = /\s*,\s*/
        var user_location = msg.split(re)
        user_id = user_location[0]
        lat = user_location[1]
        lng = user_location[2]

        var mysql = require('mysql')
        var connection = mysql.createConnection({
            host: 'localhost',
            user: 'mndoobco_sh',
            password: '5,JypNep_&yV',
            database: 'mndoobco_sh'
        });
        connection.connect();
        var sql = "UPDATE user SET latitude = " + lat + ",longitude = " + lng + " WHERE id = " + user_id;
        connection.query(sql);
        connection.end();
        console.log("location update to user " + user_id);
    })

    socket.on('connect', function (msg) {
        console.log(msg)
    })

    socket.on('join', function (name) {
        socket.name = name
        console.log(socket.name + ' joined the chat.')
    })

    socket.on('disconnect', function () {
        console.log(socket.name + ' has disconnected from the chat.' + socket.id)
    })
})

server.listen(3000, function () {
    console.log('Server listening on *:3000')
})