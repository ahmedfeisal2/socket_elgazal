var server = require('http').Server()
var io = require('socket.io')(server)

var mysql_host = "localhost";
var user = "elazalel_user";
var password = "jW2hk1=xa2KD";
var database = "elazalel_db";
var charset = "utf8mb4";


// var Redis = require('ioredis')
// var redis = new Redis()

var mysql = require('mysql');
// connection = mysql.createConnection({
//     host: mysql_host,
//     user: user,
//     password: password,
//     database: database,
//     charset: charset
// });
// connection.connect(function (err) {
//     if (err) throw err;
//     console.log("databse Connected!");
// });
var connection='';

function handleDisconnect() {
    if(connection !==''){
        connection.destroy();
        console.log('destroy first');
    }
    connection = mysql.createConnection({
        host: mysql_host,
        user: user,
        password: password,
        database: database,
        charset: charset
    });                                               // Recreate the connection, since
    // the old one cannot be reused.

    connection.connect(function(err) {              // The server is either down
        if(err) {                                     // or restarting (takes a while sometimes).
            console.log('error when connecting to db:', err);
            setTimeout(handleDisconnect, 2000); // We introduce a delay before attempting to reconnect,
        }
        console.log("databse Connected!");               // to avoid a hot loop, and to allow our node script to
    });                                     // process asynchronous requests in the meantime.
                                            // If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
        console.log('db error', err.code);

        //- The server close the connection.
        if(err.code === "PROTOCOL_CONNECTION_LOST"){
            console.log("/!\\ The server close the connection. /!\\ ("+err.code+")");
            handleDisconnect();
        }

        //- Connection in closing
        else if(err.code === "PROTOCOL_ENQUEUE_AFTER_QUIT"){
            console.log("/!\\ Connection in closing. /!\\ ("+err.code+")");
            setTimeout(handleDisconnect, 2000);
        }

        //- Fatal error : connection variable must be recreated
        else if(err.code === "PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR"){
            console.log("/!\\ Connection variable must be recreated. /!\\ ("+err.code+")");
            setTimeout(handleDisconnect, 2000);
        }

        //- Error because a connection is already being established
        else if(err.code === "PROTOCOL_ENQUEUE_HANDSHAKE_TWICE"){
            console.log("/!\\ Connection is already being established. /!\\ ("+err.code+")");
        }

        //- Anything else
        else{
            console.log("/!\\ Cannot establish a connection with the database. /!\\ ("+err.code+")");
            throw err;
        }
    });
}


io.on('connection', function (socket) {
    if(connection ===''){
        console.log('no connection to db');
        handleDisconnect();
    }else if(connection.state === 'disconnected'){
        console.log('disconnected db');
        handleDisconnect();
    }
    console.log('state => ' +connection.state)

        console.log('new user connected')
        socket.on('emit_location', function (msg) {
            // console.log(msg)

            response_type = typeof msg
            if (response_type == 'string') {
                var myobj = JSON.parse(msg)
                var output = {}
                output['event_name'] = myobj['event_name']
                output['order_id'] = myobj['order_id']
                output['lat'] = myobj['lat']
                output['lng'] = myobj['lng']
            }
            io.emit(myobj['event_name'], output)
        })

        socket.on('update_location', function (msg) {
            // console.log(msg)

            var re = /\s*,\s*/
            var user_location = msg.split(re)
            user_id = user_location[0]
            lat = user_location[1]
            lng = user_location[2]

            console.log("location update " /*+ user_id*/);

            var sql = "UPDATE user SET latitude = " + lat + ",longitude = " + lng + " WHERE id = " + user_id;
            connection.query(sql);
            var emited_data = {}
            emited_data['id'] = user_id;
            emited_data['lat'] = lat;
            emited_data['lng'] = lng;
            io.emit('event_driver_'+user_id, emited_data)
            console.log('emit data');
        })

        socket.on('track_cars', function (data) {
            sender_id = data['sender_id']
            // service_id = data['service_id']
            latitude = data['latitude']
            longitude = data['longitude']
            tracking_query = 'SELECT user.id,user.full_name,user.latitude,user.longitude FROM(SELECT *, (((acos(sin((' + latitude + " * pi() / 180)) * sin(('latitude' * pi() / 180)) + cos((" + latitude + " * pi() / 180)) * cos(('latitude' * pi() / 180)) * cos(((" + longitude + " - 'longitude') * pi() / 180)))) * 180 / pi()) * 60 * 1.1515 * 1.609344) as distance FROM user) user WHERE distance <= " + 50000 + ' AND user.type="driver_male" AND user.active = active AND user.online = true  LIMIT 5'
            // tracking_query_2 = 'SELECT id,  (3959 * acos( cos(radians(37)) * cos(radians(Y(coord))) * cos(radians(X(coord)) - radians(-122)) + sin(radians(37)) * sin(radians(Y(coord))))) AS distance FROM markers HAVING distance < 25 ORDER BY distance LIMIT 20'

            var output = connection.query(tracking_query, function (err, result, fields) {
                if (err) throw err
                tracking_result = JSON.stringify(result)
                // console.log(tracking_result)
                setInterval(function () {
                    io.emit('tracking_cars_' + sender_id, tracking_result)
                }, 5000)
            })
            connection.end()
            console.log("tracking_cars_" + sender_id + " emited");
        })

        socket.on('connect', function (msg) {
            console.log(msg)
        })

        socket.on('join', function (name) {
            socket.name = name
            console.log(socket.name + ' joined the chat.')
        })

        socket.on('disconnect', function () {
            console.log(socket.name + ' has disconnected from the chat.' + socket.id)
            // if (connection.state === 'authenticated') {
            //     connection.destroy();
            //     console.log('database closed');
            // }
        })


})

server.listen(3011, function () {
    console.log('Server listening on *:3011')
    handleDisconnect();
})
